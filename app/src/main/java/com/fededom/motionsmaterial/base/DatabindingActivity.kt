package com.fededom.motionsmaterial.base

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
 *  Es una clase abstracta para proveer el DatabingdingUtil .
 *  Pr
 */
abstract class DatabindingActivity : AppCompatActivity() {

    protected  inline fun <reified  T: ViewDataBinding> binding(@LayoutRes resId : Int ): Lazy<T> =
        lazy { DataBindingUtil.setContentView<T>(this,resId) }

}